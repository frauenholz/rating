export type TMode =
    | "equal"
    | "not-equal"
    | "below"
    | "above"
    | "between"
    | "not-between"
    | "defined"
    | "undefined";
